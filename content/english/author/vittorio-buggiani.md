---
title: "Vittorio Buggiani"
image: "blog-post-1.webp"
email: "me@vittobuggiani.com"
social :
        - icon :  ti-email   # themify icon pack : https://themify.me/themify-icons
          link : "mailto:me@vittobuggiani.com"
        - icon :  ti-github  # themify icon pack : https://themify.me/themify-icons
          link : "https://gitlab.com/vittyx"
        - icon : ti-linkedin # themify icon pack : https://themify.me/themify-icons
          link : "https://www.linkedin.com/in/vittorio-buggiani-33a31b53"

---
Telematics engineer passionate about new technologies. In my spare time I am usually looking for new IT things to experiment and break.
