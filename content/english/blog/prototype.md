---
title: "Presentation of the prototype"
date: 2021-01-13T14:51:12+01:00
author: Vittorio Buggiani
image_webp: images/blog/prototype.webp
image: images/blog/prototype.jpg
description : "Presentation of the prototype"
---

## system preparations

<br/>
These are the steps we have followed for the first measurements with the system and presentation of the prototype.

### STEP-1 : Drone check and start up

The first step is to do a complete check of the drone and start it up.

{{< figure src="/images/blog/drone.jpg" >}}

### STEP-2 : Connect network and ground control station

A Wi-Fi network must be established between the ground control station and the Drone, the drone must also be connected to the mobile control unit.

{{< figure src="/images/blog/connect1.jpg" >}}
{{< figure src="/images/blog/connect2.jpg" >}}
### STEP-3 : Place the smartphone to take the signal measurements.

Put the smartphone on the drone and start taking data from the network coverage as well as GPS data for further processing.

{{< figure src="/images/blog/smartphone.jpg" >}}

### STEP-4 : Take the signal measurements.

Carrying out test flights and taking signal measurements.

{{< figure src="/images/blog/flight.jpg" >}}

You can check this video documentation of the presentation:
{{< youtube 9wffxc519GE >}}
