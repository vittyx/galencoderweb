---
title: "We want to thank the support"
date: 2021-01-19T19:51:12+01:00
author: Vittorio Buggiani
image_webp: images/blog/LOGOESCUELA.webp
image: images/blog/LOGOESCUELA.jpg
description : "we want to thank the support"
---

## we want to thank the support.

We want to thank the support and recognition that our colleagues from the Polytechnic University of Madrid (UPM) have given us on their website and their social networks on Twitter.

{{< figure src="/images/blog/publication2.png" >}}


### Also you can check the publication in his twitter account [TwitterUPM](https://twitter.com/TelecoCampusSur/status/1351473237776080897) or in his [website](https://etsist.upm.es/)
