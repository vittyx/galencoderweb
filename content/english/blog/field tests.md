---
title: "Field tests with prototypes"
date: 2022-01-13T14:51:12+01:00
author: Vittorio Buggiani
image_webp: images/blog/droneExterior.webp
image: images/blog/droneExterior.jpg
description : "Presentation of the prototype"
---

## First field tests with functional prototype

These have been the first tests that have been carried out outdoors with a functional drone prototype.

{{< figure src="/images/blog/ExteriorKitCompleto.jpg" >}}

During these tests, data has been obtained from the mobile device to later export the NetworkCellInfo log files.

These files are available at the following [link](/images/blog/CMWFproN_20210324_134118_meas_ainf_d0_n484.csv)

{{< figure src="/images/blog/droneExterior2.jpg" >}}

The result of the route on the mobile device can be seen in the following screenshot.

{{< figure src="/images/blog/TrazadoNetworkCellInfo.jpg" >}}

The measurement points and the data can be seen in the following Google Earth screenshot with the loaded data of the route and the measurements.

{{< figure src="/images/blog/TrazadoGMaps.jpg" >}}
