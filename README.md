# Website for Galencoder Project
---

Example [Hugo] website using GitLab Pages.

Learn more about GitLab Pages at https://pages.gitlab.io and the official
documentation https://docs.gitlab.com/ce/user/project/pages/.

---

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [GitLab CI](#gitlab-ci)
- [Building locally](#building-locally)
- [GitLab User or Group Pages](#gitlab-user-or-group-pages)
- [Did you fork this project?](#did-you-fork-this-project)
- [Troubleshooting](#troubleshooting)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## GitLab CI

This project's static Pages are built by [GitLab CI][ci], following the steps
defined in [`.gitlab-ci.yml`](.gitlab-ci.yml).

## Building locally

To work locally with this project, you'll have to follow the steps below:

1. Fork, clone or download this project
1. [Install][] Hugo
1. Preview your project: `hugo server`
1. Add content
1. Generate the website: `hugo` (optional)

Read more at Hugo's [documentation][].

### Preview your site

If you clone or download this project to your local computer and run `hugo server`,
your site can be accessed under `localhost:1313/hugo/`.

The theme used is adapted from http://themes.gohugo.io/beautifulhugo/.

## GitLab User or Group Pages

To use this project as your user/group website, you will need one additional
step: just rename your project to `namespace.gitlab.io`, where `namespace` is
your `username` or `groupname`. This can be done by navigating to your
project's **Settings**.

You'll need to configure your site too: change this line
in your `config.toml`, from `"https://pages.gitlab.io/hugo/"` to `baseurl = "https://namespace.gitlab.io"`.
Proceed equally if you are using a [custom domain][post]: `baseurl = "http(s)://example.com"`.

Read more about [user/group Pages][userpages] and [project Pages][projpages].

## Did you fork this project?

If you forked this project for your own use, please go to your project's
**Settings** and remove the forking relationship, which won't be necessary
unless you want to contribute back to the upstream project.

## Troubleshooting

1. CSS is missing! That means two things:

    Either that you have wrongly set up the CSS URL in your templates, or
    your static generator has a configuration option that needs to be explicitly
    set in order to serve static assets under a relative URL.

[ci]: https://about.gitlab.com/gitlab-ci/
[hugo]: https://gohugo.io
[install]: https://gohugo.io/overview/installing/
[documentation]: https://gohugo.io/overview/introduction/
[userpages]: http://doc.gitlab.com/ee/pages/README.html#user-or-group-pages
[projpages]: http://doc.gitlab.com/ee/pages/README.html#project-pages
[post]: https://about.gitlab.com/2016/04/07/gitlab-pages-setup/#custom-domains


# Meghna Hugo Theme

Meghna Hugo  is a one page professional Hugo website template and crafted with all the necessary elements and features. Apart from the exclusive appearance, the main features of Meghna are video embedding option in the homepage, about, portfolio, service, skills, works, team, testimonial, pricing, and blog sections

<!-- table of content -->
## Table of Contents
- [Example Site](#example-site)
- [Easy Setup](#easy-setup-hugo--netlify--forestry)
- [Local Machine Setup](#local-machine-setup)
- [Reporting Issues](#reporting-issues)
- [Technical Support or Questions](#technical-support-or-questions-(paid))
- [License](#license)
- [Resources](#resources)
- [Hire Us](#hire-us)
- [More Hugo Themes](https://gethugothemes.com/hugo-themes/)

<!-- demo -->
## Example Site

<img src="https://user-images.githubusercontent.com/37659754/54068559-44d79a80-4278-11e9-9601-f58d6879989c.gif" alt="screenshot" width="100%">

Check out [Live Demo](http://demo.themefisher.com/meghna-hugo/en/)

<!-- easy setup -->
## Easy Setup (Hugo + Netlify + Forestry)
Build your website with **Meghna Hugo** theme by following these easy steps(No Coding Required!)

_Checkout this video tutorial_
<a href="https://youtu.be/ResipmZmpDU" target="_blank" title="meghna hugo installation" rel="nofollow"><img width="100%" src="https://user-images.githubusercontent.com/37659754/103056582-88270880-45c7-11eb-9124-ab17bae951ec.png"></a>

In this tutorial, we will show you to make your website live without buying any hosting and touching a single line of code.

### What you need !!
1. Git acccount (Ex: GitHub, GitLab etc ). In our case, we use Github.
2. [Netlify](https://netlify.com) account to host files and add custom domain.
3. [Forestry](https://forestry.io) account to maintain whole project without code.

### Step 1 : Fork the repository
First we will fork this [Meghna Hugo](https://github.com/themefisher/meghna-hugo) template.

### Step 2 : Add your repository in Forestry
Go to your [Forestry](https://forestry.io) account and click on `import your site now`.

**Or just click this button for import to forestry** [![import to forestry](https://assets.forestry.io/import-to-forestryK.svg)](https://app.forestry.io/quick-start?repo=themefisher/meghna-hugo&engine=hugo&version=0.60.1&config=exampleSite) **and 
declare your config.toml file from `exampleSite` directory and fill up basic settings.**

Now mark everything as done. Then go to configuration to change the base url. You can put any url but this have to be similar as netlify. So, for now, put a name which you are going to put in netlify as netlify subdomain.

### Step 3 : Setup and host website with Netlify
Here comes the last step . Go to your [netlify](https://netlify.com) account and click add new site . Choose your git repository to import your website in netlify .  And now you can see the forked `meghna hugo` theme. select it and follow the steps. Then go to `site settings` for change the site name. save it and go to `deploy` from top menu, Wait a while and click on `site preview` or just simply go to the subdomain you puted as base url.

**Or just click this button to connect netlify** [![Deploy to Netlify](https://www.netlify.com/img/deploy/button.svg)](https://app.netlify.com/start/deploy?repository=https://github.com/themefisher/meghna-hugo)

**BOOM! Your site is live.** Now you can go to forestry and add, remove or customize every setting and content.

<!-- loacal nachine setup -->
## Local Machine Setup
At the top we have shown an easy hugo installation. But if you still think you want to go with the traditional way, use the following commands:

* Clone the repository
```
$ git clone git@github.com:themefisher/meghna-hugo.git
```

* Go to exampleSite dict and run hugo server
```
$ cd meghna-hugo/exampleSite/
$ hugo server --themesDir ../..
```
Or Check out [Full Documentation](https://docs.gethugothemes.com/meghna/?ref=github).

<!-- reporting issue -->
## Reporting Issues
We use GitHub Issues as the official bug tracker for the Meghna Template. Please Search [existing issues](https://github.com/themefisher/meghna-hugo/issues). It’s possible someone has already reported the same problem.
If your problem or idea has not been addressed yet, feel free to [open a new issue](https://github.com/themefisher/meghna-hugo/issues).

<!-- support -->
## Technical Support or Questions (Paid)
If you have questions or need help integrating the product please [contact us](mailto:mehedi@themefisher.com) instead of opening an issue.

<!-- licence -->
## License
Copyright &copy; 2020 Designed by [Themefisher](https://themefisher.com) & Developed by [Gethugothemes](https://gethugothemes.com)

**Code License:** Released under the [MIT](https://github.com/themefisher/meghna-hugo/blob/master/LICENSE) license.

**Image license:** The images are only for demonstration purposes. They have their own licence, we don't have permission to share those image.

<!-- resources -->
## Resources
Some third-party plugins that we used to build this template. Please check their licence.
* **Bootstrap v4.3**: https://getbootstrap.com/docs/4.3/getting-started/introduction/
* **Jquery v3.5.1**: https://jquery.com/download/
* **Themify Icons**: https://themify.me/themify-icons
* **Google Fonts**: http://fonts.google.com/
* **Lozad**: https://apoorv.pro/lozad.js/
* **Magnific Popup**: https://dimsemenov.com/plugins/magnific-popup/
* **Slick**: https://kenwheeler.github.io/slick/
* **Shuffle**: https://vestride.github.io/Shuffle/

## Hire Us
We are available for **Hiring** for your next HUGO project. Drop us a mail at [mehedi@themefisher.com](mailto:mehedi@themefisher.com)

<!-- premium themes -->
## Premium Themes
| [![Mega-Bundle-HUGO](https://gethugothemes.com/wp-content/uploads/edd/2019/09/Mega-Bundle-HUGO.png)](https://themefisher.com/products/hugo-mega-bundle/) | [![](https://gethugothemes.com/wp-content/uploads/edd/2020/09/Reader.jpg)](https://gethugothemes.com/products/reader/) | [![](https://gethugothemes.com/wp-content/uploads/edd/2020/06/bizcraft-hugo.jpg)](https://gethugothemes.com/products/bizcraft-hugo/) |
|:---:|:---:|:---:|
| **Premium Theme Bundle (40+)**  | **Reader**  | **Bizcraft**  |



